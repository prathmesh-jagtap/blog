from django.shortcuts import render, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User
from django.views.generic import (
	ListView,
	DetailView, 
	CreateView, 
	UpdateView,
	DeleteView
)
from .models import Post

def home(request):
	context = {
		'posts': Post.objects.all()
	}

	return render (request, 'blog/home.html', context)


class PostListView(ListView):
	"""docstring for PostListView"""
	# This post list view lists the post in the homepage
	model = Post
	template_name = 'blog/home.html'	# <app>/<model>_<viewtype>.html
	context_object_name = 'posts'
	ordering = ['-date_posted']		# Prefixing the date_posted with a -ve sign, orders posts from the most recent posts
	paginate_by = 5

class UserPostListView(ListView):
	"""docstring for UserPostListView"""
	# This post list view lists the post in the homepage
	model = Post
	template_name = 'blog/user_posts.html'	# <app>/<model>_<viewtype>.html
	context_object_name = 'posts'
	paginate_by = 5

	def get_queryset(self):
		user = get_object_or_404(User, username=self.kwargs.get('username'))
		return Post.objects.filter(author=user).order_by('-date_posted')
		
class PostDetailView(DetailView):
	"""docstring for PostDetailView"""
	# This view shows the detail of a specific post
	model = Post

class PostCreateView(LoginRequiredMixin, CreateView):
	"""docstring for PostCreateView"""
	# Class view used to create new post which will contain the 
	# title and the content for the new post
	model = Post
	fields = ['title', 'content']	

	# Assigning author of the post to the current logged in user
	def form_valid(self, form):
		form.instance.author = self.request.user
		return super().form_valid(form)

class PostUpdateView(LoginRequiredMixin,UserPassesTestMixin, UpdateView):
	"""docstring for PostUpdateView"""
	model = Post
	fields = ['title', 'content']	

	# Assigning author of the post to the current logged in user
	def form_valid(self, form):
		form.instance.author = self.request.user
		return super().form_valid(form)

	def test_func(self):
		post = self.get_object()	# Getting the post object
		# Checking if the logged in user is the author of the post
		"""This prevents any user from editing other users post(s) """
		if self.request.user == post.author:
			return True
		return False

class PostDeleteView(LoginRequiredMixin,UserPassesTestMixin, DeleteView):
	"""docstring for PostDeleteView"""
	model = Post
	success_url = "/"

	def test_func(self):
		post = self.get_object()	# Getting the post object
		# Checking if the logged in user is the author of the post
		"""This prevents any user from deleting other users post(s) """
		if self.request.user == post.author:
			return True
		return False
		
		

def about(request):
	return render(request, "blog/about.html", {'title': 'About'})
